Dear CRAN members,

I submit a bug-fix-update of flexsiteboard fixing a problem with Windows line endings..

Thank you for running CRAN.

## R CMD check results

I've checked on the following platforms.

### locally:

  - R version 4.1.0 (2021-05-18) on x86_64-apple-darwin17.0 (64-bit)
  
### winbuilder:
  - R Under development (unstable) (2022-10-11 r83083 ucrt)
  - R version 4.2.2 (2022-10-31 ucrt)
  - R version 4.1.3 (2022-03-10)

### rhub:

  - Windows Server 2022, R-devel, 64 bit
  - Ubuntu Linux 20.04.1 LTS, R-release, GCC
  - Fedora Linux, R-devel, clang, gfortran

All checks except the ones, listed below,returend: 

0 errors ✔ | 0 warnings ✔ | 0 notes ✔

#### `rhub Fedora Linux, R-devel, clang, gfortran` returned:

```
* checking HTML version of manual ... NOTE
Skipping checking HTML validation: no command 'tidy' found
```

I consider this related with the test platform, not with `flexsiteboard`.

Thank you once more, 

Greetings,

Stephan